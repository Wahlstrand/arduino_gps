#pragma once

#include <stdint.h>

namespace gps
{
    class Buffer
    {
    public:
        Buffer();

        void clear();
        void add(char data);
        bool empty() const;
        bool full() const;
        size_t size() const;
        char const *raw() const;

    private:
        enum
        {
            BUFFER_SIZE = 512
        };
        char m_buffer[BUFFER_SIZE];
        char *const m_pBegin;
        char *const m_pEnd;
        char *m_pNext;
    };
}
