#pragma once

#include <stdint.h>

namespace gps
{
    struct Time
    {
        uint16_t m_hour;
        uint16_t m_min;
        uint16_t m_sec;
    };
    struct Pos
    {
        float m_lat;
        float m_lon;
        float m_altMSL;
    };
    struct GGA
    {
        Time m_time;
        Pos m_pos;
        uint16_t m_fixQuality;
        uint16_t m_numSats;
        float m_hdop;
    };
}
