#include <SoftwareSerial.h>

#include "parser.h"
#include "buffer.h"

namespace gps
{
    namespace conf
    {
        uint32_t const rxPin = 5;
        uint32_t const txPin = 4;
        uint32_t const baud = 9600;
    }

    class Environment
    {
    public:
        Environment()
            : m_gpsSerial(conf::rxPin,
                        conf::txPin),
            m_buffer()
        {
        }

        void init()
        {
            Serial.begin(conf::baud);
            m_gpsSerial.begin(conf::baud);
        }

        void update()
        {
            //Stuff all available data into the buffer
            while (m_gpsSerial.available() > 0)
            {
                char const data = m_gpsSerial.read();

                //If buffer filled up for some reason, just clear it
                //and start over
                if (m_buffer.full())
                {
                    m_buffer.clear();
                }

                //If buffer is empty, only accept start sign
                //because we don't want to start buffering in
                //the middle of a message
                if (m_buffer.empty() && data != MSG_START)
                {
                    continue;
                }

                //Add to buffer
                m_buffer.add(data);

                if (data == MSG_END)
                {
                    GGA gga;
                    if (parseGGA(m_buffer.raw(), gga))
                    {
                        char printbuf[256];
                        sprintf(printbuf, "Lat: %.6f, Lon: %.6f", gga.m_pos.m_lat, gga.m_pos.m_lon);
                        Serial.println(printbuf);
                    }
                    m_buffer.clear();
                }
            }
        }

    private:
        char const MSG_START = '$';
        char const MSG_END = '\n';

        SoftwareSerial m_gpsSerial;
        Buffer m_buffer;
    };

    Environment env;
}

void setup()
{
    gps::env.init();
}
void loop()
{
    gps::env.update();
}
