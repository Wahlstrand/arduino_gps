#include "parser.h"
#include <stdio.h>

namespace gps
{
    bool isNorth(char c)
    {
        return c == 'N';
    }
    bool isSouth(char c)
    {
        return c == 'S';
    }
    bool isWest(char c)
    {
        return c == 'W';
    }
    bool isEast(char c)
    {
        return c == 'E';
    }

    bool parseGGA(char const *buffer, GGA &out)
    {
        int latDeg = -1;
        float latMinutes = -1.0f;
        char latHem = 'x';
        int lonDeg = -1;
        float lonMinutes = -1.0f;
        char lonHem = 'x';
        float MSL = -1.0f;
        int chk = -1;

        int read = sscanf(buffer, "$GPGGA,%2hu%2hu%2hu.00,%2d%f,%c,%3d%f,%c,%hu,%hu,%f,%f,M,%f,M,,*%2x",
                        &out.m_time.m_hour,
                        &out.m_time.m_min,
                        &out.m_time.m_sec,
                        &latDeg,
                        &latMinutes,
                        &latHem,
                        &lonDeg,
                        &lonMinutes,
                        &lonHem,
                        &out.m_fixQuality,
                        &out.m_numSats,
                        &out.m_hdop,
                        &out.m_pos.m_altMSL,
                        &MSL,
                        &chk);

        if (read != 15)
        {
            return false;
        }
        if (!isNorth(latHem) && !isSouth(latHem))
        {
            return false;
        }
        if (!isWest(lonHem) && !isEast(lonHem))
        {
            return false;
        }

        out.m_pos.m_lat = (isNorth(latHem) ? +1 : -1) * (latDeg + latMinutes / 60.0f);
        out.m_pos.m_lon = (isEast(lonHem) ? +1 : -1) * (lonDeg + lonMinutes / 60.0f);

        return true;
    }
}
