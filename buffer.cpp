#pragma once

#include "buffer.h"

namespace gps
{
    Buffer::Buffer()
        : m_buffer()
        , m_pBegin(&m_buffer[0])
        , m_pEnd(&m_buffer[BUFFER_SIZE - 2])
        , m_pNext(m_pBegin)
    {
        clear();
    }
    void Buffer::clear()
    {
        memset(m_buffer, 0, sizeof(m_buffer));
        m_pNext = m_pBegin;
    }
    void Buffer::add(char data)
    {
        *m_pNext = data;
        m_pNext++;
    }
    bool Buffer::empty() const
    {
        return m_pBegin == m_pNext;
    }
    bool Buffer::full() const
    {
        return m_pNext == m_pEnd;
    }
    size_t Buffer::size() const
    {
        return m_pNext - m_pBegin;
    }
    char const *Buffer::raw() const
    {
        return m_pBegin;
    }
}
