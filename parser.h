#pragma once

#include "report.h"

namespace gps
{
    //Assumes that buffer is null terminated
    bool parseGGA(char const *buffer, GGA &out);
}
